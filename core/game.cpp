#include "game.h"

Game::Game(QObject *parent) :
    QObject(parent)
{
    _ship = new Ship(this);
}

Game::~Game()
{
    delete _ship;
}

Ship * Game::ship()
{
    return _ship;
}
