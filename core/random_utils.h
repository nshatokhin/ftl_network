#ifndef RANDOM_UTILS_H
#define RANDOM_UTILS_H

#include <cstdlib>
#include <ctime>

inline void initialize_random_generator()
{
    srand(time(NULL));
}

inline double closed_interval_rand(double x0, double x1)
{
    return x0 + (x1 - x0) * rand() / ((double) RAND_MAX);
}

inline double unified_random()
{
    return closed_interval_rand(0, 2) - 1;
}

inline int integer_rand(int x0, int x1)
{
    return x0 + rand()%(x1 - x0 + 1);
}

inline int binary_rand()
{
    return integer_rand(0, 1);
}

#endif // RANDOM_UTILS_H
