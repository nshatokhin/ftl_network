CONFIG += qt plugin

INCLUDEPATH += $$PWD

SOURCES += \
    core/engineplugin.cpp \
    core/engine.cpp \
    core/gameserver.cpp \
    core/gameclient.cpp \
    core/communicationmanager.cpp \
    core/communicationprotocol.cpp \
    core/ship/reactor.cpp \
    core/ship/shields.cpp \
    core/ship/hull.cpp \
    core/ship/system.cpp \
    core/ship/ship.cpp \
    core/game.cpp \
    core/ship/compartment.cpp \
    core/ship/compartmentcell.cpp \
    core/ship/door.cpp \
    core/ship/compartmentsconnection.cpp \
    core/ship/layout.cpp \
    core/ship/connection.cpp \
    core/ship/doorscontrol.cpp

HEADERS += \
    core/engineplugin.h \
    core/engine.h \
    core/random_utils.h \
    core/gameserver.h \
    core/gameclient.h \
    core/communicationmanager.h \
    core/communicationprotocol.h \
    core/ship/reactor.h \
    core/ship/shields.h \
    core/ship/hull.h \
    core/ship/system.h \
    core/ship/ship.h \
    core/game.h \
    core/ship/compartment.h \
    core/ship/compartmentcell.h \
    core/ship/door.h \
    core/ship/compartmentsconnection.h \
    core/ship/layout.h \
    core/ship/connection.h \
    core/ship/doorscontrol.h
