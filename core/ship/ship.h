#ifndef SHIP_H
#define SHIP_H

#include <QObject>

#include "hull.h"
#include "layout.h"
#include "reactor.h"
#include "shields.h"

class Ship : public QObject
{
    Q_OBJECT

    Q_PROPERTY(Hull * hull READ hull NOTIFY hullChanged)
    Q_PROPERTY(Layout * layout READ layout NOTIFY layoutChanged)
    Q_PROPERTY(Reactor * reactor READ reactor NOTIFY reactorChanged)
    Q_PROPERTY(Shields * shields READ shields NOTIFY shieldsChanged)

public:
    explicit Ship(QObject *parent = 0);
    ~Ship();

public: //seters and geters
    Hull * hull();
    Layout * layout();
    Reactor * reactor();
    Shields * shields();

signals:
    void hullChanged(Hull * hull);
    void layoutChanged(Layout * layout);
    void reactorChanged(Reactor * reactor);
    void shieldsChanged(Shields * shields);

public slots:

private:
    Hull * _hull;
    Layout * _layout;
    Reactor * _reactor;
    Shields * _shields;

};

#endif // SHIP_H
