#ifndef CONNECTION_H
#define CONNECTION_H

#include <QObject>

#include "compartment.h"
#include "compartmentcell.h"

class Connection : public QObject
{
    Q_OBJECT

    Q_PROPERTY(Compartment * compartment READ compartment WRITE setCompartment NOTIFY compartmentChanged)
    Q_PROPERTY(CompartmentCell * cell READ cell WRITE setCell NOTIFY cellChanged)
public:
    explicit Connection(QObject *parent = 0);

    Compartment * compartment();
    CompartmentCell * cell();
    void setCompartment(Compartment * compartment);
    void setCell(CompartmentCell * cell);
    
signals:
    void compartmentChanged();
    void cellChanged();

private:
    Compartment * _compartment;
    CompartmentCell * _cell;
    
public slots:
    
};

#endif // CONNECTION_H
