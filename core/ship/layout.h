#ifndef LAYOUT_H
#define LAYOUT_H

#include <QObject>
#include <QQmlListProperty>

#include "compartment.h"

class Layout : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QQmlListProperty<Compartment> compartments READ compartments WRITE setCompartments NOTIFY compartmentsChanged)

public:
    explicit Layout(QObject *parent = 0);
    ~Layout();

    QQmlListProperty<Compartment> compartments();
    void setCompartments(QQmlListProperty<Compartment> compartments);

signals:
    void compartmentsChanged(QQmlListProperty<Compartment> compartments);

public slots:

private:
    QList<Compartment*> _compartments;
    QList<CompartmentCell*> _cells;

};

#endif // LAYOUT_H
