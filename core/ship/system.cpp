#include "system.h"

System::System(QObject *parent) :
    QObject(parent)
{
}

int System::systemLevel()
{
    return _systemLevel;
}

int System::systemEnergy()
{
    return _systemEnergy;
}

int System::systemDamage()
{
    return _systemDamage;
}

int System::systemBlocking()
{
    return _systemBlocking;
}

void System::setSystemLevel(int level)
{
    _systemLevel = level;

    emit systemLevelChanged(_systemLevel);
}

void System::setSystemEnergy(int energy)
{
    _systemEnergy = energy;

    if(_systemEnergy <= 0)
        this->setSystemPowered(false);
    else
        this->setSystemPowered(true);

    emit systemEnergyChanged(_systemEnergy);
}

void System::setSystemDamage(int damage)
{
    _systemDamage = damage;

    emit systemDamageChanged(_systemDamage);
}

void System::setSystemBlocking(int blocking)
{
    _systemBlocking = blocking;

    emit systemBlockingChanged(_systemBlocking);
}

void System::damageSystem(int damage)
{
    _systemDamage += damage;

    this->getEnergy(damage);

    if(_systemDamage >= _systemLevel)
    {
        _systemDamage = _systemLevel;
        this->setSystemDestroyed(true);
    }

    emit systemDamageChanged(_systemDamage);
}

void System::repairSystem(int damage)
{
    _systemDamage -= damage;

    if(_systemDamage < _systemLevel)
        this->setSystemDestroyed(false);

    if(_systemDamage < 0)
        _systemDamage = 0;

    emit systemDamageChanged(_systemDamage);
}

int System::putEnergy(int energy)
{
    if(_systemEnergy < _systemLevel)
    {
        _systemEnergy += energy;

        if(_systemEnergy > _systemLevel)
            energy = energy - (_systemEnergy - _systemLevel);

        if(_systemEnergy > 0)
            this->setSystemPowered(true);

        emit systemEnergyChanged(_systemEnergy);
    }
    else
        energy = 0;

    return energy;
}

int System::getEnergy(int energy)
{
    if(_systemEnergy > 0)
    {
        _systemEnergy -= energy;

        if(_systemEnergy < 0)
        {
            energy += _systemEnergy;
            _systemEnergy = 0;
        }

        if(_systemEnergy == 0)
            this->setSystemPowered(false);

        emit systemEnergyChanged(_systemEnergy);
    }
    else
        energy = 0;

    return energy;
}

int System::scaleValue()
{
    return _scaleValue;
}

void System::setScaleValue(int value)
{
    _scaleValue = value;

    emit scaleValueChanged(_scaleValue);
}

bool System::systemDestroyed()
{
    return _systemDestroyed;
}

void System::setSystemDestroyed(bool destroyed)
{
    if(_systemDestroyed != destroyed)
    {
        _systemDestroyed = destroyed;

        emit systemDestroyedChanged(_systemDestroyed);
    }
}

bool System::systemPowered()
{
    return _systemPowered;
}

void System::setSystemPowered(bool powered)
{
    if(_systemPowered != powered)
    {
        _systemPowered = powered;

        emit systemPoweredChanged(powered);
    }
}
