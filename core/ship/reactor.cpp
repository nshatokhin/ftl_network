#include "reactor.h"

Reactor::Reactor(QObject *parent) :
    QObject(parent)
{
    _generatedEnergy = 10;
    _freeEnergy = 5;
}

int Reactor::generatedEnergy()
{
    return _generatedEnergy;
}

int Reactor::freeEnergy()
{
    return _freeEnergy;
}

void Reactor::setGeneratedEnergy(int energy)
{
    _generatedEnergy = energy;

    emit generatedEnergyChanged();
}

void Reactor::setFreeEnergy(int energy)
{
    _freeEnergy = energy;

    emit freeEnergyChanged();
}

void Reactor::takeEnergy(int energy)
{
    _freeEnergy -= energy;

    if(_freeEnergy < 0)
        _freeEnergy = 0;

    emit freeEnergyChanged();
}

void Reactor::returnEnergy(int energy)
{
    _freeEnergy += energy;

    if(_freeEnergy > _generatedEnergy)
        _freeEnergy = _generatedEnergy;

    emit freeEnergyChanged();
}
