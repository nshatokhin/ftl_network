#include "hull.h"

Hull::Hull(QObject *parent) :
    QObject(parent)
{
    _hullIntegrity = 5;
    _maxHull = 30;
}

int Hull::hullIntegrity()
{
    return _hullIntegrity;
}

void Hull::setHullIntegrity(int hull)
{
    _hullIntegrity = hull;

    emit hullIntegrityChanged(_hullIntegrity);
}

int Hull::maxHull()
{
    return _maxHull;
}

void Hull::setMaxHull(int max)
{
    _maxHull = max;

    emit maxHullChanged(_maxHull);
}

void Hull::repairHull(int hull)
{
    _hullIntegrity += hull;

    if(_hullIntegrity > _maxHull)
        _hullIntegrity = _maxHull;

    emit hullIntegrityChanged(_hullIntegrity);
}

void Hull::damageHull(int hull)
{
    _hullIntegrity -= hull;

    if(_hullIntegrity < 0)
        _hullIntegrity = 0;

    emit hullIntegrityChanged(_hullIntegrity);
}
