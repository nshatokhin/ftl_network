#ifndef DOOR_H
#define DOOR_H

#include <QObject>
#include <QList>

#include "connection.h"

class Door : public QObject
{
    Q_OBJECT

    Q_PROPERTY(bool closed READ closed WRITE setClosed NOTIFY closedChanged)
    Q_PROPERTY(int level READ level WRITE setLevel NOTIFY levelChanged)
    Q_PROPERTY(int hp READ hp WRITE setHp NOTIFY hpChanged)

public:
    explicit Door(QObject *parent = 0);

    bool closed();
    void setClosed(bool closed);
    int level();
    void setLevel(int level);
    int hp();
    void setHp(int hp);

signals:
    void closedChanged(bool closed);
    void levelChanged(int level);
    void hpChanged(int hp);

public slots:

private:
    bool _closed;
    int _hp;
    int _level;

    QList<Connection *> _connections; // TODO: установить связи из Layout

};

#endif // DOOR_H
