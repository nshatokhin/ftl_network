#ifndef COMPARTMENTCELL_H
#define COMPARTMENTCELL_H

#include <QObject>

#include "connection.h"

class CompartmentCell : public QObject
{
    Q_OBJECT
public:
    explicit CompartmentCell(QObject *parent = 0);
    CompartmentCell(const CompartmentCell&);

private:
    Connection * _left, * _right, * _front, * _back;

signals:

public slots:

};

#endif // COMPARTMENTCELL_H
