#ifndef SYSTEM_H
#define SYSTEM_H

#include <QObject>

class System : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int systemLevel READ systemLevel WRITE setSystemLevel NOTIFY systemLevelChanged)
    Q_PROPERTY(int systemEnergy READ systemEnergy WRITE setSystemEnergy NOTIFY systemEnergyChanged)
    Q_PROPERTY(int systemDamage READ systemDamage WRITE setSystemDamage NOTIFY systemDamageChanged)
    Q_PROPERTY(int systemBlocking READ systemBlocking WRITE setSystemBlocking NOTIFY systemBlockingChanged)

    Q_PROPERTY(int scaleValue READ scaleValue WRITE setScaleValue NOTIFY scaleValueChanged)
    Q_PROPERTY(bool systemDestroyed READ systemDestroyed WRITE setSystemDestroyed NOTIFY systemDestroyedChanged)
    Q_PROPERTY(bool systemPowered READ systemPowered WRITE setSystemPowered NOTIFY systemPoweredChanged)
public:
    explicit System(QObject *parent = 0);

    int systemLevel();
    int systemEnergy();
    int systemDamage();
    int systemBlocking();
    void setSystemLevel(int level);
    void setSystemEnergy(int energy);
    void setSystemDamage(int damage);
    void setSystemBlocking(int blocking);

    bool systemDestroyed();
    void setSystemDestroyed(bool destroyed);
    bool systemPowered();
    void setSystemPowered(bool powered);

    int scaleValue();
    void setScaleValue(int value);

    Q_INVOKABLE void damageSystem(int damage);
    Q_INVOKABLE void repairSystem(int damage);
    Q_INVOKABLE int putEnergy(int energy);
    Q_INVOKABLE int getEnergy(int energy);

signals:
    void systemLevelChanged(int level);
    void systemEnergyChanged(int energy);
    void systemDamageChanged(int damage);
    void systemBlockingChanged(int blocking);
    void systemDestroyedChanged(bool destroyed);
    void systemPoweredChanged(bool powered);

    void scaleValueChanged(int value);

public slots:

private:
    int _systemLevel;
    int _systemEnergy;
    int _systemDamage;
    int _systemBlocking;
    bool _systemDestroyed;
    bool _systemPowered;

    int _scaleValue;
};

#endif // SYSTEM_H
