#include "compartment.h"

Compartment::Compartment(int posX, int posY, int width, int height, System *system, QObject *parent) :
    QObject(parent)
{
    _posX = posX;
    _posY = posY;
    _width = width;
    _height = height;
}

QQmlListProperty<CompartmentCell> Compartment::cells()
{
    return _cells;
}

void Compartment::setCells(QQmlListProperty<CompartmentCell> cells)
{
    _cells = cells;

    emit cellsChanged(_cells);
}

QQmlListProperty<Door *> Compartment::doors()
{
    return _doors;
}

void Compartment::setDoors(QQmlListProperty<Door *> doors)
{
    _doors = doors;

    emit doorsChanged(_doors);
}

int Compartment::posX()
{
    return _posX;
}

void Compartment::setPosX(int posX)
{
    _posX = posX;

    emit posXChanged(_posX);
}

int Compartment::posY()
{
    return _posY;
}

void Compartment::setPosY(int posY)
{
    _posY = posY;

    emit posXChanged(_posY);
}

int Compartment::width()
{
    return _width;
}

void Compartment::setWidth(int width)
{
    _width = width;

    emit widthChanged(_width);
}

int Compartment::height()
{
    return _height;
}

void Compartment::setHeight(int height)
{
    _height = height;

    emit heightChanged(_height);
}
