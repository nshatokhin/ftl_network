#include "ship.h"

Ship::Ship(QObject *parent) :
    QObject(parent)
{
    _hull = new Hull(this);
    _layout = new Layout(this);
    _reactor = new Reactor(this);
    _shields = new Shields(this);

    // TODO: сделать конфиг и загрузку параметров из конфига
}

Ship::~Ship()
{
    delete _hull;
    delete _layout;
    delete _reactor;
    delete _shields;
}

Hull * Ship::hull()
{
    return _hull;
}

Layout * Ship::layout()
{
    return _layout;
}

Reactor * Ship::reactor()
{
    return _reactor;
}

Shields * Ship::shields()
{
    return _shields;
}
