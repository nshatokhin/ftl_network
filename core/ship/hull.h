#ifndef HULL_H
#define HULL_H

#include <QObject>

class Hull : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int hullIntegrity READ hullIntegrity WRITE setHullIntegrity NOTIFY hullIntegrityChanged)
    Q_PROPERTY(int maxHull READ maxHull WRITE setMaxHull NOTIFY maxHullChanged)
public:
    explicit Hull(QObject *parent = 0);

    int hullIntegrity();
    void setHullIntegrity(int hull);
    int maxHull();
    void setMaxHull(int max);

    Q_INVOKABLE void repairHull(int hull);
    Q_INVOKABLE void damageHull(int hull);

signals:
    void hullIntegrityChanged(int hull);
    void maxHullChanged(int max);

public slots:

private:
    int _hullIntegrity;
    int _maxHull;

};

#endif // HULL_H
