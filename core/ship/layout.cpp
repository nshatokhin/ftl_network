#include "layout.h"

Layout::Layout(QObject *parent) :
    QObject(parent)
{
    _compartments.push_back(new Compartment(0, 2, 1, 2, NULL, this));
    _compartments.push_back(new Compartment(1, 1, 2, 1, NULL, this));
    _compartments.push_back(new Compartment(1, 2, 2, 2, NULL, this));
    _compartments.push_back(new Compartment(1, 4, 2, 1, NULL, this));
    _compartments.push_back(new Compartment(3, 1, 2, 1, NULL, this));
    _compartments.push_back(new Compartment(3, 4, 2, 1, NULL, this));
    _compartments.push_back(new Compartment(4, 2, 2, 2, NULL, this));
    _compartments.push_back(new Compartment(6, 0, 2, 1, NULL, this));
    _compartments.push_back(new Compartment(6, 1, 2, 2, NULL, this));
    _compartments.push_back(new Compartment(6, 3, 2, 2, NULL, this));
    _compartments.push_back(new Compartment(6, 5, 2, 1, NULL, this));
    _compartments.push_back(new Compartment(8, 1, 2, 2, NULL, this));
    _compartments.push_back(new Compartment(8, 3, 2, 2, NULL, this));
    _compartments.push_back(new Compartment(10, 2, 2, 1, NULL, this));
    _compartments.push_back(new Compartment(10, 3, 2, 1, NULL, this));
    _compartments.push_back(new Compartment(12, 2, 2, 2, NULL, this));
    _compartments.push_back(new Compartment(14, 2, 1, 2, NULL, this));

    // TODO: сделать управление созданием отсеков и ячеек
    // связать отсеки и ячейки в графы
    // сделать загрузку из конфига корабля

    // Создаем ячейки
    // Создаем отсеки
    // Создаем двери
}

Layout::~Layout()
{
    Compartment * compartment;

    while(!_compartments.isEmpty())
    {
        compartment = _compartments.takeLast();
        delete compartment;
    }
}

QQmlListProperty<Compartment> Layout::compartments()
{
    return QQmlListProperty<Compartment>(this, _compartments);
}

void Layout::setCompartments(QQmlListProperty<Compartment> compartments)
{
//    _compartments.clear();
//    _compartments = QList<Compartment>(compartments);

    emit compartmentsChanged(QQmlListProperty<Compartment>(this, _compartments));
}
