#include "door.h"

Door::Door(QObject *parent) :
    QObject(parent)
{
}

bool Door::closed()
{
    return _closed;
}

void Door::setClosed(bool closed)
{
    _closed = closed;

    emit closedChanged(_closed);
}

int Door::level()
{
    return _level;
}

void Door::setLevel(int level)
{
    _level = level;

    emit levelChanged(_level);
}

int Door::hp()
{
    return _hp;
}

void Door::setHp(int hp)
{
    _hp = hp;

    emit hpChanged(_hp);
}
