#ifndef COMPARTMENT_H
#define COMPARTMENT_H

#include <QObject>
#include <QList>
#include <QQmlListProperty>

#include "compartmentcell.h"
#include "door.h"
#include "system.h"

class Compartment : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QQmlListProperty<CompartmentCell> cells READ cells WRITE setCells NOTIFY cellsChanged)
    Q_PROPERTY(QQmlListProperty<Door *> doors READ doors WRITE setDoors NOTIFY doorsChanged)
    Q_PROPERTY(int posX READ posX WRITE setPosX NOTIFY posXChanged)
    Q_PROPERTY(int posY READ posY WRITE setPosY NOTIFY posYChanged)
    Q_PROPERTY(int width READ width WRITE setWidth NOTIFY widthChanged)
    Q_PROPERTY(int height READ height WRITE setHeight NOTIFY heightChanged)

public:
    explicit Compartment(int posX = 0, int posY = 0, int width = 1, int height = 1, System * system = NULL, QObject *parent = 0);

    QQmlListProperty<CompartmentCell> cells();
    void setCells(QQmlListProperty<CompartmentCell> cells);
    QQmlListProperty<Door *> doors();
    void setDoors(QQmlListProperty<Door *> doors);

    int posX();
    void setPosX(int posX);
    int posY();
    void setPosY(int posY);
    int width();
    void setWidth(int width);
    int height();
    void setHeight(int height);

signals:
    void cellsChanged(QQmlListProperty<CompartmentCell> cells);
    void doorsChanged(QQmlListProperty<Door *> doors);
    void posXChanged(int posX);
    void posYChanged(int posY);
    void widthChanged(int width);
    void heightChanged(int height);

public slots:

private:
    QQmlListProperty<CompartmentCell> _cells;
    QQmlListProperty<Door *> _doors;
    int _posX, _posY;
    int _width, _height;
};

#endif // COMPARTMENT_H
