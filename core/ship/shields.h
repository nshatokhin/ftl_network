#ifndef SHIELDS_H
#define SHIELDS_H

#include <QObject>

#include "system.h"

class Shields : public System
{
    Q_OBJECT

    Q_PROPERTY(int shieldsCount READ shieldsCount WRITE setShieldsCount NOTIFY shieldsCountChanged)
    Q_PROPERTY(int shieldsActive READ shieldsActive WRITE setShieldsActive NOTIFY shieldsActiveChanged)
public:
    explicit Shields(QObject *parent = 0);

    int shieldsCount();
    int shieldsActive();
    void setShieldsCount(int value);
    void setShieldsActive(int value);

    Q_INVOKABLE void takeOffShields(int value);
    Q_INVOKABLE void restoreShields(int value);

signals:
    void shieldsCountChanged(int value);
    void shieldsActiveChanged(int value);

public slots:

private:
    int _shieldsCount;
    int _shieldsActive;

};

#endif // SHIELDS_H
