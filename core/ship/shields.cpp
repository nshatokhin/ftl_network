#include "shields.h"

Shields::Shields(QObject *parent) :
    System(parent)
{
    _shieldsActive = 1;
    _shieldsCount = 3;
}

int Shields::shieldsActive()
{
    return _shieldsActive;
}

int Shields::shieldsCount()
{
    return _shieldsCount;
}

void Shields::setShieldsActive(int value)
{
    _shieldsActive = value;

    emit shieldsActiveChanged(_shieldsActive);
}

void Shields::setShieldsCount(int value)
{
    _shieldsCount = value;

    emit shieldsCountChanged(_shieldsCount);
}

void Shields::takeOffShields(int value)
{
    _shieldsActive -= value;

    if(_shieldsActive < 0)
        _shieldsActive = 0;

    emit shieldsActiveChanged(_shieldsActive);
}

void Shields::restoreShields(int value)
{
    _shieldsActive += value;

    if(_shieldsActive > _shieldsCount)
        _shieldsActive = _shieldsCount;

    emit shieldsCountChanged(_shieldsCount);
}
