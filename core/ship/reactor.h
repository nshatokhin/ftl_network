#ifndef REACTOR_H
#define REACTOR_H

#include <QObject>

class Reactor : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int generatedEnergy READ generatedEnergy WRITE setGeneratedEnergy NOTIFY generatedEnergyChanged)
    Q_PROPERTY(int freeEnergy READ freeEnergy WRITE setFreeEnergy NOTIFY freeEnergyChanged)
public:
    explicit Reactor(QObject *parent = 0);

    int generatedEnergy();
    int freeEnergy();
    void setGeneratedEnergy(int energy);
    void setFreeEnergy(int energy);

    void takeEnergy(int energy);
    void returnEnergy(int energy);

signals:
    void freeEnergyChanged();
    void generatedEnergyChanged();

public slots:

private:
    int _generatedEnergy;
    int _freeEnergy;

};

#endif // REACTOR_H
