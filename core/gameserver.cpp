#include "gameserver.h"
#include <QDebug>

GameServer::GameServer(QObject *parent) :
    QObject(parent)
{
    _isHost = false;

    server = new QTcpServer(this);
    //client = new QTcpSocket(this);

    connect(server, SIGNAL(newConnection()), this, SLOT(incomingConnection()));
}

GameServer::~GameServer()
{
    delete server;
    //delete client;
}

void GameServer::startServer()
{
    if(server->listen(QHostAddress::Any, SERVER_PORT))
    {
        qDebug() << "listening";
        _isHost = true;

        emit serverStarted();
    }
    else
    {
        qDebug() << "can't listen";
        emit couldNotStartServer();
    }
}

void GameServer::stopServer()
{
    server->close();
    qDebug() << "stoped";
    emit serverStoped();
}

void GameServer::incomingConnection()
{
    qDebug() << "connected";
    client = server->nextPendingConnection();

    connect(client, SIGNAL(disconnected()), this, SLOT(clientDisconnected()));
    connect(client, SIGNAL(readyRead()), this, SLOT(readSocket()));

    emit connectionEstablished();
}

void GameServer::clientDisconnected()
{
    qDebug() << "client disconnected";

    disconnect(client, SIGNAL(disconnected()), this, SLOT(clientDisconnected()));
    disconnect(client, SIGNAL(readyRead()), this, SLOT(readSocket()));

    client->deleteLater();

    emit connectionClosed();
}

void GameServer::readSocket()
{
    QByteArray data = client->readAll();

    emit gotMessage(data);
}

bool GameServer::isHost()
{
    return _isHost;
}

void GameServer::sendMessage(QByteArray data)
{
    client->write(data);
}
