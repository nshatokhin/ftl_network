#ifndef GAME_H
#define GAME_H

#include <QObject>

#include "ship/ship.h"

class Game : public QObject
{
    Q_OBJECT

    Q_PROPERTY(Ship * ship READ ship NOTIFY shipChanged)

public:
    explicit Game(QObject *parent = 0);
    ~Game();

public: //setters and getters
    Ship * ship();

signals:
    void shipChanged(Ship * ship);

public slots:

private:
    Ship * _ship;
};

#endif // GAME_H
