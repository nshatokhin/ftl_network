#ifndef COMMUNICATIONMANAGER_H
#define COMMUNICATIONMANAGER_H

#include <QObject>

#include "gameclient.h"
#include "gameserver.h"

class CommunicationManager : public QObject
{
    Q_OBJECT
public:
    explicit CommunicationManager(QObject *parent = 0);

signals:
    void gotMessage(QByteArray data);
    void serverStarted();
    void serverStoped();
    void connectedToServer();
    void clientConnected();
    void disconnectedFromServer();
    void cantStartServer();
    void cantConnectToServer();
    void clientDisconnected();

public slots:
    void startServer();
    void stopServer();
    void connectToServer(QString address);
    void disconnectFromServer();
    void sendMessage(QByteArray data);

private:
    GameServer * server;
    GameClient * client;

};

#endif // COMMUNICATIONMANAGER_H
