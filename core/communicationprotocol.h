#ifndef COMMUNICATIONPROTOCOL_H
#define COMMUNICATIONPROTOCOL_H

#include <QObject>

class CommunicationProtocol : public QObject
{
    Q_OBJECT
public:
    explicit CommunicationProtocol(QObject *parent = 0);

signals:
    void sendMessage(QByteArray data);

public slots:
    void gotMessage(QByteArray data);

};

#endif // COMMUNICATIONPROTOCOL_H
