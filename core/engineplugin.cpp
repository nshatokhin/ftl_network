#include "engineplugin.h"
#include "engine.h"
#include "game.h"

#include "core/ship/compartment.h"
#include "core/ship/ship.h"
#include "core/ship/hull.h"
#include "core/ship/layout.h"
#include "core/ship/reactor.h"
#include "core/ship/shields.h"

EnginePlugin::EnginePlugin(QQuickItem *parent) :
    QQmlExtensionPlugin(parent)
{
}

void EnginePlugin::registerTypes(const char *uri)
{
    // @uri Engine
    qmlRegisterType<Engine>(uri, 1, 0, "Engine");

    // @uri Engine
    qmlRegisterType<Game>(uri, 1, 0, "GameBackend");

    // @uri Engine
    qmlRegisterType<Ship>(uri, 1, 0, "ShipBackend");

    // @uri Engine
    qmlRegisterType<Hull>("engine", 1, 0, "HullBackend");

    // @uri Engine
    qmlRegisterType<Reactor>("engine", 1, 0, "ReactorBackend");

    // @uri Engine
    qmlRegisterType<Shields>("engine", 1, 0, "ShieldsBackend");

    // @uri Engine
    qmlRegisterType<Layout>("engine", 1, 0, "LayoutBackend");

    // @uri Engine
    qmlRegisterType<Compartment>("engine", 1, 0, "CompartmentBackend");
}

void EnginePlugin::initialize()
{
    //EngineHolder::createInstance();
}

void EnginePlugin::deinitialize()
{
    //EngineHolder::destroyInstance();
}
