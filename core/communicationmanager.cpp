#include "communicationmanager.h"

CommunicationManager::CommunicationManager(QObject *parent) :
    QObject(parent)
{
    server = new GameServer(this);
    client = new GameClient(this);

    connect(server, SIGNAL(serverStarted()), this, SIGNAL(serverStarted()));
    connect(server, SIGNAL(serverStoped()), this, SIGNAL(serverStoped()));
    connect(server, SIGNAL(couldNotStartServer()), this, SIGNAL(cantStartServer()));
    connect(server, SIGNAL(gotMessage(QByteArray)), this, SIGNAL(gotMessage(QByteArray)));
    connect(server, SIGNAL(connectionEstablished()), this, SIGNAL(clientConnected()));
    connect(server, SIGNAL(connectionClosed()), this, SIGNAL(clientDisconnected()));

    connect(client, SIGNAL(connectionEstablished()), this, SIGNAL(connectedToServer()));
    connect(client, SIGNAL(couldNotConnect()), this, SIGNAL(cantConnectToServer()));
    connect(client, SIGNAL(gotMessage(QByteArray)), this, SIGNAL(gotMessage(QByteArray)));
    connect(client, SIGNAL(connectionClosed()), this, SIGNAL(disconnectedFromServer()));
}

void CommunicationManager::startServer()
{
    server->startServer();
}

void CommunicationManager::stopServer()
{
    server->stopServer();
}

void CommunicationManager::connectToServer(QString address)
{
    client->connectToServer(address);
}

void CommunicationManager::disconnectFromServer()
{
    client->disconnectFromServer();
}

void CommunicationManager::sendMessage(QByteArray data)
{
    if(server->isHost())
        server->sendMessage(data);
    else
        client->sendMessage(data);
}
