#ifndef EnginePLUGIN_H
#define EnginePLUGIN_H

#include <QtQml/QQmlExtensionPlugin>
#include <QtQuick/QQuickItem>

class EnginePlugin : public QQmlExtensionPlugin
{
    Q_OBJECT
public:
    explicit EnginePlugin(QQuickItem *parent = 0);
    
    void registerTypes(const char *uri);

    void initialize();
    void deinitialize();
};

#endif // EnginePLUGIN_H
