#include <QDebug>

#include "random_utils.h"
#include "engine.h"

Engine::Engine(QObject *parent) :
    QObject(parent)//QQuickItem(parent)
{
    _game = new Game(this);

    _gameState = MAIN_MENU;

    /*_server = new GameServer();
    _client = new GameClient();

    _server->startServer();
    _client->connectToServer("127.0.0.1");*/
    communicationManager = new CommunicationManager(this);
    communicationProtocol = new CommunicationProtocol(this);

    connect(communicationManager, SIGNAL(gotMessage(QByteArray)),
                communicationProtocol, SLOT(gotMessage(QByteArray)));
    connect(communicationProtocol, SIGNAL(sendMessage(QByteArray)),
                communicationManager, SLOT(sendMessage(QByteArray)));

    connect(communicationManager, SIGNAL(serverStarted()), this, SIGNAL(serverStarted()));
    connect(communicationManager, SIGNAL(serverStoped()), this, SIGNAL(serverStoped()));
    connect(communicationManager, SIGNAL(connectedToServer()), this, SIGNAL(connectedToServer()));
    connect(communicationManager, SIGNAL(disconnectedFromServer()), this, SIGNAL(disconnectedFromServer()));
    connect(communicationManager, SIGNAL(clientConnected()), this, SIGNAL(clientConnected()));
    connect(communicationManager, SIGNAL(cantStartServer()), this, SIGNAL(couldNotStartServer()));
    connect(communicationManager, SIGNAL(cantConnectToServer()), this, SIGNAL(couldNotConnectToServer()));
    connect(communicationManager, SIGNAL(clientDisconnected()), this, SIGNAL(clientDisconnected()));

    connect(this, SIGNAL(startGame()), SLOT(onStartGame()));
    connect(this, SIGNAL(stopGame()), SLOT(onStopGame()));
    connect(this, SIGNAL(pauseGame(bool)), SLOT(onPauseGame(bool)));
}

Engine::~Engine()
{
    delete _game;

    delete communicationManager;
    delete communicationProtocol;
}

Game * Engine::game()
{
    return _game;
}

void Engine::_initializeGame()
{

}

void Engine::_deinitializeGame()
{

}

void Engine::startServer()
{
    communicationManager->startServer();
}

void Engine::stopServer()
{
    communicationManager->stopServer();
}

void Engine::connectToServer(QString address)
{
    communicationManager->connectToServer(address);
}

void Engine::disconnectFromServer()
{
    communicationManager->disconnectFromServer();
}

int Engine::gameState()
{
    return _gameState;
}

void Engine::setGameState(int state)
{
    _gameState = state;

    emit gameStateChanged(_gameState);
}

int Engine::hullState()
{
    return _hullState;
}

void Engine::setHullState(int hullState)
{
    _hullState = hullState;

    emit hullStateChanged(_hullState);
}

int Engine::shieldsState()
{
    return _shieldsState;
}

void Engine::setShieldsState(int shieldsState)
{
    _shieldsState = shieldsState;

    emit shieldsStateChanged(_shieldsState);
}

int Engine::shieldsCount()
{
    return _shieldsCount;
}

void Engine::setShieldsCount(int shieldsCount)
{
    _shieldsCount = shieldsCount;

    emit shieldsCountChanged(_shieldsCount);
}

void Engine::onStartGame()
{

}

void Engine::onStopGame()
{

}

void Engine::onPauseGame(bool pause)
{

}

QML_DECLARE_TYPE(Engine)
