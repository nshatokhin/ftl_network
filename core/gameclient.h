#ifndef GAMECLIENT_H
#define GAMECLIENT_H

#include <QObject>
#include <QHostAddress>
#include <QTcpSocket>

class GameClient : public QObject
{
    Q_OBJECT
public:
    explicit GameClient(QObject *parent = 0);
    ~GameClient();

signals:
    void couldNotConnect();
    void connectionEstablished();
    void connectionClosed();
    void gotMessage(QByteArray data);

public slots:
    void connectToServer(QString serverIp);
    void disconnectFromServer();
    void sendMessage(QByteArray data);

private:
    QTcpSocket * client;
    QHostAddress * address;

private slots:
    void incommingMessage();

};

#endif // GAMECLIENT_H
