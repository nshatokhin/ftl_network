#ifndef GAMESERVER_H
#define GAMESERVER_H

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>

class GameServer : public QObject
{
    Q_OBJECT
public:
    static const quint16 SERVER_PORT = 6000;

    explicit GameServer(QObject *parent = 0);
    ~GameServer();

    bool isHost();

signals:
    void couldNotStartServer();
    void serverStarted();
    void serverStoped();
    void connectionEstablished();
    void connectionClosed();
    void gotMessage(QByteArray data);

public slots:
    void startServer();
    void stopServer();
    void sendMessage(QByteArray data);

private:
    QTcpServer * server;
    QTcpSocket * client;

    bool _isHost;

private slots:
    void incomingConnection();
    void readSocket();
    void clientDisconnected();
};

#endif // GAMESERVER_H
