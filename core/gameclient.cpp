#include "gameclient.h"
#include "gameserver.h"
#include <QDebug>

GameClient::GameClient(QObject *parent) :
    QObject(parent)
{
    client = new QTcpSocket(this);
    address = new QHostAddress();

    connect(client, SIGNAL(readyRead()), this, SLOT(incommingMessage()));
}

GameClient::~GameClient()
{
    delete client;
    delete address;
}

void GameClient::connectToServer(QString serverIp)
{
    qDebug() << "Trying connect to server: " << serverIp;
    address->setAddress(serverIp);
    client->connectToHost(*address, GameServer::SERVER_PORT, QIODevice::ReadWrite);

    if(client->waitForConnected())
    {
        qDebug() << "connected to server";

        emit connectionEstablished();
    }
    else
    {
        qDebug() << "couldn't connect";

        emit couldNotConnect();
    }
}

void GameClient::disconnectFromServer()
{
    client->disconnectFromHost();

    qDebug() << "disconnected from server";

    emit connectionClosed();
}

void GameClient::sendMessage(QByteArray data)
{
    client->write(data);
}

void GameClient::incommingMessage()
{
    QByteArray data = client->readAll();

    emit gotMessage(data);
}
