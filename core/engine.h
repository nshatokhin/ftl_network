#ifndef Engine_H
#define Engine_H

#include <QTimer>
#include <QtQuick/QQuickItem>

#include "communicationmanager.h"
#include "communicationprotocol.h"
#include "game.h"

class Engine : public QObject//QQuickItem
{
    Q_OBJECT

    //Q_PROPERTY(int hullState READ hullState WRITE setHullState NOTIFY hullStateChanged)
    //Q_PROPERTY(int shieldsState READ shieldsState WRITE setShieldsState NOTIFY shieldsStateChanged)
    //Q_PROPERTY(int shieldsCount READ shieldsCount WRITE setShieldsCount NOTIFY shieldsCountChanged)
    Q_PROPERTY(Game * game READ game NOTIFY gameChanged)

public:
    explicit Engine(/*QQuickItem*/QObject *parent = 0);
    ~Engine();

    enum STATES {
        MAIN_MENU = 0,
        IN_GAME,
        GAME_PAUSED,
        GAME_WON,
        GAME_LOST,
        INTRO,
        OUTRO,
        CREDITS
    };

public:    // Property setters and getters
    Game * game();

    int gameState();
    void setGameState(int state);

    int hullState();
    void setHullState(int hullState);
    int shieldsState();
    void setShieldsState(int shieldsState);
    int shieldsCount();
    void setShieldsCount(int shieldsCount);

    Q_INVOKABLE void startServer();
    Q_INVOKABLE void connectToServer(QString address);
    Q_INVOKABLE void stopServer();
    Q_INVOKABLE void disconnectFromServer();

private:
    Game * _game;

    int _gameState;
    int _hullState;
    int _shieldsState;
    int _shieldsCount;

    CommunicationManager * communicationManager;
    CommunicationProtocol * communicationProtocol;

    void _initializeGame();
    void _deinitializeGame();

signals:    // Property signals
    void gameChanged(Game * game);

    void gameStateChanged(int state);

    void hullStateChanged(int hullState);
    void shieldsStateChanged(int shieldsState);
    void shieldsCountChanged(int shieldsCount);
    
signals:
    void startGame();
    void gameStarted();
    void stopGame();
    void gameStoped();
    void pauseGame(bool pause);
    void gamePauseState(bool pause);

    void serverStarted();
    void serverStoped();
    void connectedToServer();
    void clientConnected();
    void clientDisconnected();
    void disconnectedFromServer();
    void couldNotConnectToServer();
    void couldNotStartServer();

    void victory();
    void lose();
    
private slots:
    void onStartGame();
    void onStopGame();
    void onPauseGame(bool pause);

private:

    //Q_DISABLE_COPY(Engine)
};

#endif // Engine_H
