#include <QtWidgets/QApplication>
#include <QtQuick/QQuickView>

#include <QtQml/QQmlEngine>

#include "core/engineplugin.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    EnginePlugin plugin;

    plugin.initialize();

    // @uri Engine
    plugin.registerTypes("Engine");

    QQuickView view;

    view.setResizeMode(QQuickView::SizeRootObjectToView);
    QQmlEngine::connect(view.engine(), SIGNAL(quit()), &app, SLOT(quit()));
    view.setSource(QUrl("qrc:/main.qml"));

    //view.showFullScreen();
    view.show();

    int result = app.exec();

    plugin.deinitialize();

    return result;
}
