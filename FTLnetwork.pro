#-------------------------------------------------
#
# Project created by QtCreator 2013-09-28T14:34:33
#
#-------------------------------------------------

QT       += core gui widgets qml quick

TARGET = FTLnetwork

TEMPLATE = app

include(core/core.pri)

SOURCES += src/main.cpp

RESOURCES += \
    game_resources.qrc

OTHER_FILES += \
    qml/desktop_main.qml \
    qml/MainMenu.qml \
    qml/Game.qml \
    qml/ConnectToServer.qml \
    qml/StartServer.qml \
    qml/PrepareToGame.qml \
    qml/ConfirmationDialog.qml \
    qml/InformationWindow.qml \
    qml/Reactor.qml \
    qml/ReactorCell.qml \
    qml/Shields.qml \
    qml/ShieldsCell.qml \
    qml/Hull.qml \
    qml/HullCell.qml \
    qml/System.qml \
    qml/Ship.qml \
    qml/Layout.qml \
    qml/Compartment.qml \
    qml/Screen.qml

HEADERS +=
