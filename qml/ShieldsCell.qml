import QtQuick 2.0
import Ubuntu.Components 0.1

Rectangle {
    id: cell
    width: units.gu(2)
    height: units.gu(2)
    border.color: "blue"

    states: [
            State {
                name: "charged"
                PropertyChanges { target: cell; color: "blue" }
            },
            State {
                name: "destoyed"
                PropertyChanges { target: cell; color: "transparent" }
            }
        ]
}
