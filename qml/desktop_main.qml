import QtQuick 2.0
import Ubuntu.Components 0.1

import Engine 1.0

MainView {
    width: units.gu(100)
    height: units.gu(75)

    PageStack {
        id: rootStack
        anchors.fill: parent

        Component.onCompleted:
        {
            engine.createInstance;
            push(mainMenu);
        }

        Page {
            id: mainMenu
            visible: false

            MainMenu
            {
                onStartServer:
                {
                    engine.startServer();

                    //gameInstance.startGame(width, height);
                }

                onConnectToServer:
                {
                    rootStack.push(connectToServer)
                }

                onQuit:
                {
                    engine.destroyInstance;

                    Qt.quit();
                }
            }
        }

        Page {
            id: prepareToGame
            visible: false

            PrepareToGame
            {
                onStartGame:
                {
                    rootStack.pop();
                    rootStack.push(game)
                }
            }
        }

        Page {
            id: startServer
            visible: false

            StartServer
            {
                onAbortConnection:
                {
                    //rootStack.pop();
                    //rootStack.push(mainMenu);
                    engine.stopServer();
                }
            }
        }

        Page {
            id: connectToServer
            visible: false

            ConnectToServer
            {
                onCancelConnection:
                {
                    //engine.disconnectFromServer();
                    rootStack.pop();
                    rootStack.push(mainMenu);
                }

                onConnectToServer:
                {
                    engine.connectToServer(address);
                }
            }
        }

        Page {
            id: game
            visible: false

            Game
            {

            }
        }
    }

    Engine {
        id: engine

        onServerStarted:
        {
            rootStack.push(startServer);
        }

        onServerStoped:
        {
            rootStack.pop();
        }

        onConnectedToServer:
        {
            rootStack.pop();
            rootStack.push(prepareToGame);
        }

        onClientConnected:
        {
            rootStack.pop();
            rootStack.push(prepareToGame);
        }

        onClientDisconnected:
        {
            engine.stopServer();
            //rootStack.pop();
            //rootStack.push(mainMenu);
        }

        onCouldNotStartServer:
        {
            info.title = "Error!"
            info.text = "Could not start server"
            info.show();
        }

        onCouldNotConnectToServer:
        {
            info.title = "Error!"
            info.text = "Could not connect to server"
            info.show();
        }

        onDisconnectedFromServer:
        {
            info.title = "Success!"
            info.text = "Connection closed"
            info.show();
        }
    }

    InformationWindow {
        id: info
    }
}
