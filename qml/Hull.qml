import QtQuick 2.0
import Ubuntu.Components 0.1
import engine 1.0

Rectangle {
    id: hull

    property var backend: engine.game.ship.hull

    Row
    {
        spacing: units.gu(0.5)

        Repeater
        {
            id: hullScale
            model: backend.maxHull

            HullCell
            {
                id: hullCell
                state: (index < backend.hullIntegrity)?"intact":"damaged";
            }
        }
    }

    Connections
    {
        target: backend

        onHullIntegrityChanged:
        {
            hullScale.update();
        }

        onMaxHullChanged:
        {
            hullScale.update();
        }
    }
}
