import QtQuick 2.0
import Ubuntu.Components 0.1
import Ubuntu.Components.Popups 0.1

Item {
    id: parentItem
    signal confirmed;
    signal canceled;

    signal show(variant caller);

    property string title: "Exit Game"
    property string text: "Are you sure that you want to exit?"

    Component {
         id: dialogComponent

         Dialog {
             id: dialog
             title: parentItem.title
             text: parentItem.text

             Button {
                 text: "cancel"
                 color: "green"
                 onClicked:
                 {
                     PopupUtils.close(dialog);
                     canceled();
                 }
             }
             Button {
                 text: "confirm"
                 color: "red"
                 onClicked:
                 {
                     PopupUtils.close(dialog);
                     confirmed();
                 }
             }
         }
    }

    onShow:
    {
        PopupUtils.open(dialogComponent, caller)
    }
}

