import QtQuick 2.0
import QtGraphicalEffects 1.0

Rectangle {
    id: system
    width: units.gu(3.5)
    height: units.gu(3.5)
    radius: units.gu(1.75)
    color: "green"

    property var backend: null

    property int centerX: width / 2
    property int centerY: height / 2

    Connections
    {
        target: backend

    }

    Repeater
    {
        id: systemScale
    }

    MouseArea
    {
        anchors.fill: parent



        function mouseOver(mouseX, mouseY)
        {
            return ((pow(mouseX - parent.centerX, 2) + pow(mouseY - parent.centerY, 2)) <= pow(parent.radius, 2));
        }
    }

}
