import QtQuick 2.0
import Ubuntu.Components 0.1
import engine 1.0

Rectangle {
    id: shields

    property var backend: engine.game.ship.shields

    Row
    {
        id: shieldsContainer
        spacing: units.gu(0.5)

        Repeater
        {
            id: shieldsScale

            model: backend.shieldsCount

            ShieldsCell
            {
                id: shieldsCell
                state: (index < backend.shieldsActive)?"charged":"destroyed"
            }
        }
    }

    Connections
    {
        target: backend

        onShieldsActiveChanged:
        {
            shieldsScale.update();
        }

        onShieldsCountChanged:
        {
            shieldsScale.update();
        }
    }
}
