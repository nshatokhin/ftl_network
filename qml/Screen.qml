import QtQuick 2.0

Rectangle {
    border.color: "green"
    width: ship.width
    height: ship.height

    Ship
    {
        id: ship
        x: 100
        y: 200
        horizontal: false
    }
}
