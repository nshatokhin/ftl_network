import QtQuick 2.0
import engine 1.0

Rectangle {
    id: reactor
    width: energyScale.width
    height: energyScale. height

    property var backend: engine.game.ship.reactor

    Repeater
    {
        id: energyScale
        model: backend.generatedEnergy

        ReactorCell
        {
            id: reactorCell
            y: -(height + index * (height + units.gu(0.5)))
            state: (index < backend.freeEnergy)?"free":"used"
        }
    }

    Connections
    {
        target: backend

        onGeneratedEnergyChanged:
        {
            energyScale.update()
        }

        onFreeEnergyChanged:
        {
            energyScale.update();
        }
    }
}
