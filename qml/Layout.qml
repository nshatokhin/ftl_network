import QtQuick 2.0

Rectangle {
    id: layout

    property var backend: null
    property bool horizontal: true

    Repeater
    {
        model: backend.compartments.length

        Compartment
        {
            backend: layout.backend.compartments[index]
            x: horizontal?(posX * cellSize):(posY * cellSize)
            y: horizontal?(posY * cellSize):(-posX * cellSize - height)
            horizontal: layout.horizontal
        }
    }

    Connections
    {
        target: backend
    }
}
