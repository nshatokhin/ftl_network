import QtQuick 2.0
import engine 1.0

Rectangle {
    anchors.fill: parent
    color: "black"

    Hull
    {

    }

    Shields
    {
        y: 50
    }

    Reactor
    {
        id: reactor
        y: parent.height
    }

    System
    {
        x: units.gu(3) + units.gu(1.5)
        y: parent.height - height

        backend: engine.game.ship.shields
    }

    Ship
    {
        x: 50
        y: 200
        horizontal: true
    }

    Screen
    {
        x: 500
        y: 300
    }
}
