import QtQuick 2.0
import Ubuntu.Components 0.1

Rectangle {
    anchors.fill: parent

    signal connectToServer(string address);
    signal cancelConnection();

    Column {
        anchors.centerIn: parent
        width: units.gu(40)
        spacing: units.gu(2)

        Item {
            id: addressInput
            property alias text: input.text
            width: parent.width;
            height: units.gu(4);

            BorderImage {
                source: "images/lineedit.sci"
                anchors.fill: parent
            }
            TextInput {
                id: input
                color: "#151515"; selectionColor: "green"
                font.pixelSize: 16; font.bold: true
                width: parent.width-16
                maximumLength: 16
                anchors.centerIn: parent
                focus: true

                text: "127.0.0.1"
            }
        }

        Row {
            spacing: units.gu(2)
            width: parent.width

            Button {
                width: (parent.width - parent.spacing) / 2;
                id: connectBtn
                text: "Connect"

                onClicked:
                {
                    console.log(addressInput.text);
                    connectToServer(addressInput.text)
                }
            }

            Button {
                width: (parent.width - parent.spacing) / 2;
                id: cancelBtn
                text: "Cancel"

                onClicked:
                {
                    cancelConnection();
                }
            }
        }
    }
}
