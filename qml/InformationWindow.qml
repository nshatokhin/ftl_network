import QtQuick 2.0
import Ubuntu.Components 0.1
import Ubuntu.Components.Popups 0.1

Item {
    id: parentItem
    signal confirmed;
    signal canceled;

    signal show;

    property string title: "Info"
    property string text: "Information window"

    Component {
         id: infoComponent

         Dialog {
             id: info
             title: parentItem.title
             text: parentItem.text

             Button {
                 text: "OK"
                 color: "green"
                 onClicked:
                 {
                     PopupUtils.close(info);
                     confirmed();
                 }
             }
         }
    }

    onShow:
    {
        PopupUtils.open(infoComponent)
    }
}

