import QtQuick 2.0
import Ubuntu.Components 0.1

Rectangle {
    anchors.fill: parent

    signal abortConnection;

    Column {
        anchors.centerIn: parent
        width: units.gu(40)
        spacing: units.gu(2)

        Text {
            width: parent.width
            text: "Awaiting connection..."
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        Button {
            id: cancelBtn
            width: parent.width
            text: "Cancel"

            onClicked:
            {
                abortConnection();
            }
        }
    }
}
