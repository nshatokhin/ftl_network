import QtQuick 2.0
import Ubuntu.Components 0.1

Rectangle {
    width: 0
    height: 0
    color: "green"
    border.color: "blue"

    property int posX: 0
    property int posY: 0

    property int cellSize: 30
    property var backend: null

    property bool horizontal: true

    Connections
    {
        target: backend
    }

    onBackendChanged:
    {
        width = horizontal?(backend.width * cellSize):(backend.height * cellSize);
        height = horizontal?(backend.height * cellSize):(backend.width * cellSize);
        posX = backend.posX;
        posY = backend.posY;
    }

    onHorizontalChanged:
    {
        width = horizontal?(backend.width * cellSize):(backend.height * cellSize);
        height = horizontal?(backend.height * cellSize):(backend.width * cellSize);
        posX = backend.posX;
        posY = backend.posY;
    }
}
