import QtQuick 2.0
import Ubuntu.Components 0.1

Rectangle {
    anchors.fill: parent

    signal startGame;

    Button
    {
        id: startGameBtn
        anchors.centerIn: parent
        text: "Start game"

        onClicked:
        {
            startGame();
        }
    }
}
