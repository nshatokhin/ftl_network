import QtQuick 2.0
import Ubuntu.Components 0.1

Rectangle {
    id: cell
    width: units.gu(2)
    height: units.gu(3.5)
    border.color: "green"

    states: [
            State {
                name: "intact"
                PropertyChanges { target: cell; color: "green" }
            },
            State {
                name: "damaged"
                PropertyChanges { target: cell; color: "transparent" }
            }
        ]
}
