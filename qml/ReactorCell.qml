import QtQuick 2.0
import Ubuntu.Components 0.1

Rectangle {
    id: cell
    width: units.gu(3)
    height: units.gu(1.5)
    border.color: "green"

    states: [
            State {
                name: "free"
                PropertyChanges { target: cell; color: "green" }
            },
            State {
                name: "used"
                PropertyChanges { target: cell; color: "transparent" }
            }
        ]
}
