import QtQuick 2.0

Rectangle {

    property alias horizontal: layout.horizontal

    Layout
    {
        id: layout
        backend: engine.game.ship.layout
    }
}
