import QtQuick 2.0
import Ubuntu.Components 0.1
import Ubuntu.Components.Popups 0.1

Rectangle {
    id: mainMenu
    anchors.fill: parent
    color: "transparent"

    signal startServer;
    signal connectToServer;
    signal quit;

    Column {
        anchors.centerIn: parent
        width: units.gu(40)
        spacing: units.gu(2)

        Button {
            objectName: "startServer"
            width: parent.width

            text: "Start Server"

            onClicked: {
                startServer();
            }
        }
        Button {
            objectName: "connectToServer"
            width: parent.width

            text: "Connect to Server"

            onClicked: {
                connectToServer();
            }
        }

        Button {
            id: quitBtn
            objectName: "quitBtn"
            width: parent.width
            text: "Exit"

            onClicked: {
                confirmExitDialog.confirmed.connect(quit);
                confirmExitDialog.show(quitBtn);
            }
        }
    }

    ConfirmationDialog
    {
        id: confirmExitDialog
    }
}
